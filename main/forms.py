from django.forms import ModelForm
from django.core.exceptions import ValidationError
from main.models import FeedbackMessage


class FeedbackForm(ModelForm):
    class Meta:
        model = FeedbackMessage

    def clean(self):
        phone = self.instance.user_phone
        email = self.instance.user_email
        if not phone and not email:
                raise ValidationError("Укажите Ваш email или телефон")
        return self.cleaned_data